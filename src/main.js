// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// eslint-disable-next-line import/no-extraneous-dependencies
import Vue from 'vue';
import Select from './Select.vue';
import Dev from './Dev.vue';

Vue.component('lb-vue-select', Select);

new Vue({
    render: (h) => h(Dev),
}).$mount('#app');
