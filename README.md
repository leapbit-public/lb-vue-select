<br><br>

<h3>lb-vue-select</h3>

<br>
<p><img src="https://www.leapbit.com/branding/logo.png" style="width: 300px;"></p>

<br>

## Screenshot

<p><img src="https://gitlab.com/leapbit-public/lb-vue-select/raw/master/screen.png" alt="lb-vue-select" ></p>

## Description

<p align="center">
  Vue2.js select with serverside pagination, multiselect option
</p>

<br><br><br><br><br>

## Install

``` bash
npm install lb-vue-select --save
```

Include plugin in your `main.js` file.

```js
import Vue from 'vue'
import lbVueSelect from 'lb-vue-select';
Vue.component('lb-vue-select', lbVueSelect);
```

## Using

template code

```html
<template>
    <lb-vue-select v-model="value" :list="list" :pagination="pagination" display-field="value" value-field="key" multiselect="true" empty="true" search-placeholder="Search" items-placeholder="Items per page"></lb-vue-select>
</template>
```

script code

```js
export default {
    data(){
        return {
            value: '1',
            //value: '1,2' - in case of multiselect
            totalCount: 300, //optional
            list: [
                {
                    key: '1',
                    value: 'First option'
                },
                {
                    key: '2',
                    value: 'Second option'
                }
            ],
            pagination: {
                query: '',
                currentPage: 1,
                limit: 20,
                initialId: [],
                searchTimeout: false
            }
        }
    },
    watch:
    {
        'pagination.query'(query)
        {
            //when someone queries, do the store reload here
        },
        'pagination.currentPage'(page)
        {
            //when someone changes the page, do the store reload here
        }
    }
};
```
<br><br>


## Available settings

| Property | Type | Required | Description |
| :---------------- | :-- | :-- | :-- |
| value (v-model) | string | * | Value string, in case of multiselect, separated by comma ',' |
| list | [Object] | * | Array of objects containing values |
| display-field | string | | Display field key form list item object (Defaults to key) |
| display-func | function | | If you want to combine multiple fields to display text, you can specify custom function which receives one argument (item) |
| value-field | string | | Value field key form list item object (Defaults to value) |
| multiselect | boolean| | If you want to enable multiselect (Defaults to false) |
| select-all | string| | Text that will be placed next to checkbox for selecting all options. Available only along with :multiselect="true". If not provided, checkbox will not be available |
| all-selected-text | string| | Text that will be displayed in input instead of all checked options when there are more then 5 selected options. |
|searchBox | boolean| | searchBox will appear if set to true. |
| empty | boolean | | If you want to enable deselection of value (Defaults to false) |
| pagination | object | | In case you use pagination, object as in example |
| totalCount | int |  | In case you use pagination in select, total count of all items |
| searchPlaceholder | string |  | Placeholder for pagination search, if using vue-i18n it will use key 'SEARCH', if omited will use "Search"
| itemsPlaceholder | string |  | Placeholder for pagination items per page selector, if using vue-i18n it will use key 'ITEMSPERPAGE', if omited, will use "Items per page" |
| css | string | | Class to use on input (Defaults to form-control) |
| disabled | boolean | | Disables input if set to true (Defaults to false) |

<br><br>

## Available events

| Event | Arguments | Description |
| :---------------- | :-- | :-- |
| click | - |  |
| change | value | New value |

<br><br>