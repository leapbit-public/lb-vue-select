import vue from 'rollup-plugin-vue'; // Handle .vue SFC files
import buble from 'rollup-plugin-buble'; // Transpile/polyfill with reasonable browser support
import commonjs from 'rollup-plugin-commonjs';

export default {
    input: 'wrapper.js', // Path relative to package.json
    output: {
        name: 'LbVueSelect',
        exports: 'named',
    },
    external: ['bootstrap-vue', 'lb-vue-pagination', 'vue-click-outside'],
    plugins: [
        commonjs(),
        vue({
            css: true, // Dynamically inject css as a <style> tag
            compileTemplate: true, // Explicitly convert template to render function
        }),
        buble({
            transforms: {
                dangerousForOf: true,
            },
        }),
    ],
};
